angular.module('bodyFatCalculator', [])
    .controller('BodyFatController', function() {
        var calculator = this;

        calculator.selectOpts               = ['centimeters', 'inches'];
        calculator.heightSelect             = calculator.selectOpts[0];
        calculator.circumferenceWaistSelect = calculator.selectOpts[0];
        calculator.circumferenceNeckSelect  = calculator.selectOpts[0];
        calculator.circumferenceHipsSelect  = calculator.selectOpts[0];

        calculator.calculate = function() {
            HS     = calculator.heightSelect;
            cWS    = calculator.circumferenceWaistSelect;
            cNS    = calculator.circumferenceNeckSelect;
            cHS    = calculator.circumferenceHipsSelect;
            var H  = ((HS === 'centimeters') ? calculator.height : calculator.height * 2.54) || 0;
            var cW = ((cWS === 'centimeters') ? calculator.circumferenceWaist : calculator.circumferenceWaist * 2.54) || 0;
            var cN = ((cNS === 'centimeters') ? calculator.circumferenceNeck : calculator.circumferenceNeck * 2.54) || 0;
            var cH = ((cHS === 'centimeters') ? calculator.circumferenceHips : calculator.circumferenceHips * 2.54) || 0;

            if (cH === 0) {
                // %Fat = 86.010 * LOG(abdomen - neck) - 70.041 * LOG(height) + 30.30
                return 86.010 * calculator.log10(cW - cN) - 70.041 * calculator.log10(H) + 30.30;
            } else {
                // %Fat = 163.205 * LOG(abdomen + hip - neck) - 97.684 * LOG(height) - 104.912
                return 163.205 * calculator.log10(cW + cH - cN) - 97.684 * calculator.log10(H) - 104.912;
            }
        };

        calculator.log10 = function (val) {
            return Math.log(val) / Math.LN10;
        };
    });
